package ru.greenbeard.sqlinteraction;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Spanned;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Vector;

import ru.greenbeard.sqlinteraction.sql.DatabaseHandler;
import ru.greenbeard.sqlinteraction.sql.TablePeopleContract.*;

public class MainActivity extends AppCompatActivity {

    private TextInputEditText nameField = null;
    private TextInputEditText surnameField = null;
    private DatabaseHandler dbHandler = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHandler = new DatabaseHandler(this);
        nameField = findViewById(R.id.NameInput);
        surnameField = findViewById(R.id.SurnameInput);
        nameField.setText("ABOBA");
    }

    public void Insert(View view) {
        String sql = String.format(
                "INSERT INTO %s (%s, %s) VALUES ('%s', '%s');",
                People.table,
                People.name,
                People.surname,
                nameField.getText().toString(),
                surnameField.getText().toString()
        );
        try
        {
            SQLiteDatabase db = dbHandler.getWritableDatabase();
            db.execSQL(sql);
            nameField.setText("");
            surnameField.setText("");
        }
        catch(SQLException ex)
        {
            SQLiteDatabase db = dbHandler.getReadableDatabase();
        }
        ViewPeople(view);
    }

    public void Delete(View view) {
        String sql = String.format(
                "DELETE FROM %s "
                        + "WHERE %s = '%s' "
                        + "AND %s = '%s';",
                People.table,
                People.name,
                nameField.getText().toString(),
                People.surname,
                surnameField.getText().toString()
        );
        try
        {
            SQLiteDatabase db = dbHandler.getWritableDatabase();
            db.execSQL(sql);
            nameField.setText("");
            surnameField.setText("");
            ViewPeople(view);
        }
        catch(SQLException ex)
        {
            SQLiteDatabase db = dbHandler.getReadableDatabase();
        }
    }

    public void ViewPeople(View view) {
        Vector<String> result = new Vector<>();
        SQLiteDatabase db = dbHandler.getReadableDatabase();
        String sql = String.format("SELECT * FROM %s;", People.table);
        Cursor cursor = db.rawQuery(sql, null);
        try
        {
            String name;
            String surname;
            String info;
            int nameIndex = cursor.getColumnIndex(People.name);
            int surnameIndex = cursor.getColumnIndex(People.surname);
            while (cursor.moveToNext())
            {
                name = cursor.getString(nameIndex);
                surname = cursor.getString(surnameIndex);
                info = String.format("Фамилия: %1$s\n Имя: %2$s\n", surname, name);
                result.add(info);
            }
        }
        finally
        {
            cursor.close();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, result);
        ListView listView = (ListView) findViewById(R.id.ViewPeople);
        listView.setAdapter(adapter);
    }
}