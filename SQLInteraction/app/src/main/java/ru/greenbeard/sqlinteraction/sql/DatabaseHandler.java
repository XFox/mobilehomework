package ru.greenbeard.sqlinteraction.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import ru.greenbeard.sqlinteraction.sql.TablePeopleContract.*;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String dbName = "DBFile.db";

    public DatabaseHandler(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = String.format(
                "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + "%s TEXT, "
                        + "%s TEXT);",
                People.table,
                People._ID,
                People.name,
                People.surname);
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        StringBuilder sql = new StringBuilder();
        sql.append("DROP TABLE IF EXISTS ").append(People.table);
        db.execSQL(sql.toString());
        onCreate(db);
    }
}
