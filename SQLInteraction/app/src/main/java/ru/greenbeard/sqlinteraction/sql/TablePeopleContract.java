package ru.greenbeard.sqlinteraction.sql;

import android.provider.BaseColumns;

public class TablePeopleContract{

    public class People implements BaseColumns  {
        public static final String table = "People";
        public static final String _ID = BaseColumns._ID;
        public static final String name = "Name";
        public static final String surname = "Surname";
    }

}
