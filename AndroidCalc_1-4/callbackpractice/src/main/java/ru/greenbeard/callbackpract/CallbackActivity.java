package ru.greenbeard.callbackpract;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class CallbackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callback);
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, );
        startActivity(intent);
    }
}