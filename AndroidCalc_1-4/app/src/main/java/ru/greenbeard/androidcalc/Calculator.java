package ru.greenbeard.androidcalc;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import java.util.Vector;

public class Calculator {
    private static String currentValue = "";
    private static Vector<Double> values = new Vector<Double>();
    private static Vector<Character> operations = new Vector<Character>();
    private static Boolean noNewInput = false;
    private static Activity activity;

    public static void setActivity(Activity act) {
        activity = act;
    }

    public static void numberClick(View view) {
        int id = view.getId();
        if(id == R.id.buttonDot) {
            if(currentValue.indexOf('.') == -1) {
                if (currentValue.equals("")) currentValue = "0.";
                else currentValue += ".";
            }
        }
        if(id == R.id.buttonZero) currentValue += "0";
        if(id == R.id.buttonOne) currentValue += "1";
        if(id == R.id.buttonTwo) currentValue += "2";
        if(id == R.id.buttonThree) currentValue += "3";
        if(id == R.id.buttonFour) currentValue += "4";
        if(id == R.id.buttonFive) currentValue += "5";
        if(id == R.id.buttonSix) currentValue += "6";
        if(id == R.id.buttonSeven) currentValue += "7";
        if(id == R.id.buttonEight) currentValue += "8";
        if(id == R.id.buttonNine) currentValue += "9";
        noNewInput = false;
        updateText();
    }

    public static void clearText(View view) {
        currentValue = "";
        values.clear();
        operations.clear();
        updateText();
    }

    public static void operationClick(View view) {
        if(noNewInput) return;
        noNewInput = true;
        int id = view.getId();
        if(currentValue.equals("")) currentValue = "0.";
        values.add(Double.valueOf(currentValue));
        currentValue = "";
        if(id == R.id.buttonPlus) operations.add('+');
        if(id == R.id.buttonMinus) operations.add('-');
        if(id == R.id.buttonMultiply) operations.add('*');
        if(id == R.id.buttonDivide) operations.add('/');
        if(id == R.id.buttonSin || id == R.id.buttonCos || id == R.id.buttonTan) {
            if(operations.size() == 0) {
                if(id == R.id.buttonSin) operations.add('s');
                if(id == R.id.buttonCos) operations.add('c');
                if(id == R.id.buttonTan) operations.add('t');
            }
            else if(operations.lastElement() != 's' &&
                    operations.lastElement() != 'c' &&
                    operations.lastElement() != 't') {
                if(id == R.id.buttonSin) operations.add('s');
                if(id == R.id.buttonCos) operations.add('c');
                if(id == R.id.buttonTan) operations.add('t');
            }
            noNewInput = false;
        }
    }

    public static void calculate(View view) {
        Double intermediateValue = 0.;
        if(currentValue.equals("")) currentValue = "0.";
        values.add(Double.valueOf(currentValue));
        int operationIndex = -1;
        while(values.size() != 1 && operations.size() != 0) {
            operationIndex = operations.indexOf('t');
            if (operationIndex != -1) {
                intermediateValue = Math.tan(values.elementAt(operationIndex));
                values.set(operationIndex, intermediateValue);
                operations.remove(operationIndex);
                continue;
            }
            operationIndex = operations.indexOf('c');
            if (operationIndex != -1) {
                intermediateValue = Math.cos(values.elementAt(operationIndex));
                values.set(operationIndex, intermediateValue);
                operations.remove(operationIndex);
                continue;
            }
            operationIndex = operations.indexOf('s');
            if (operationIndex != -1) {
                intermediateValue = Math.sin(values.elementAt(operationIndex));
                values.set(operationIndex, intermediateValue);
                operations.remove(operationIndex);
                continue;
            }
            operationIndex = operations.indexOf('*');
            if (operationIndex != -1) {
                intermediateValue = values.elementAt(operationIndex) * values.elementAt(operationIndex + 1);
                updateVectors(operationIndex, intermediateValue);
                continue;
            }
            operationIndex = operations.indexOf('/');
            if (operationIndex != -1) {
                intermediateValue = values.elementAt(operationIndex) / values.elementAt(operationIndex + 1);
                updateVectors(operationIndex, intermediateValue);
                continue;
            }
            operationIndex = operations.indexOf('+');
            if (operationIndex != -1) {
                intermediateValue = values.elementAt(operationIndex) + values.elementAt(operationIndex + 1);
                updateVectors(operationIndex, intermediateValue);
                continue;
            }
            operationIndex = operations.indexOf('-');
            if (operationIndex != -1) {
                intermediateValue = values.elementAt(operationIndex) - values.elementAt(operationIndex + 1);
                updateVectors(operationIndex, intermediateValue);
            }
        }
        currentValue = String.valueOf(values.firstElement());
        updateText();
        values.clear();
        operations.clear();
    }
    private static void updateVectors(int operationIndex, double intermediateValue) {
        values.set(operationIndex, intermediateValue);
        values.remove(operationIndex + 1);
        operations.remove(operationIndex);
    }
    public static void updateText() {
        TextView text = activity.findViewById(R.id.inputNumber);
        text.setText(currentValue);
    }
}
