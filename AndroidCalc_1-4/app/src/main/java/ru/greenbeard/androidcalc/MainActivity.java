package ru.greenbeard.androidcalc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;
import java.util.Vector;
import java.util.function.Function;

public class MainActivity extends AppCompatActivity {

    static Boolean checked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Calculator.setActivity(this);
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
    }

    public void operationClick(View view) {
        Calculator.operationClick(view);
    }

    public void numberClick(View view) {
        Calculator.numberClick(view);
    }

    public void ClearText(View view) {
        Calculator.clearText(view);
    }

    public void Calculate(View view) {
        Calculator.calculate(view);
    }

    public void SwitchStyles() {
        if(checked) {
            setContentView(R.layout.activity_main);
        }
        else setContentView(R.layout.activity_main_shrink);
        checked = !checked;
        Calculator.updateText();
        setSupportActionBar(findViewById(R.id.my_toolbar));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.base_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.ModeSwitch) {
            SwitchStyles();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}