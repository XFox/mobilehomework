package ru.greenbeard.quiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

class Question {
    public String question;
    public Integer correctAnswer;
    public String answer1;
    public String answer2;

    public Question(String q, Integer ca, String a1, String a2) {
        question = q;
        correctAnswer = ca;
        answer1 = a1;
        answer2 = a2;
    }
}

public class MainActivity extends AppCompatActivity {
    int currentIndex = 0;
    int correctAnswers = 0;
    ArrayList<Question> questions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            currentIndex = extras.getInt("ru.greenbeard.currentIndex");
            correctAnswers = extras.getInt("ru.greenbeard.correctAnswers");
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        questions = new ArrayList<>();
        questions.add(new Question("Квас?", 1, "КВ-1С", "КВ-122"));
        questions.add(new Question("Валенок?", 1, "Об. 212", "VK 75.01 (K)"));
        questions.add(new Question("Танк горит?", 1, "FV-215b", "Т-62А"));
        SetupQuestion();
    }

    void SetupQuestion() {
        TextView question = findViewById(R.id.question);
        Button answer1 = findViewById(R.id.Answer1);
        Button answer2 = findViewById(R.id.Answer2);
        Question item = questions.get(currentIndex);
        question.setText(item.question);
        answer1.setText(item.answer1);
        answer2.setText(item.answer2);
    }
    public void OnAnswerSelect(View view) {
        int id = view.getId();
        if(id == R.id.Answer1) {
            if(questions.get(currentIndex).correctAnswer == 1) correctAnswers++;
        }
        if(id == R.id.Answer2) {
            if(questions.get(currentIndex).correctAnswer == 2) correctAnswers++;
        }
        currentIndex++;
        StartNextActivity();
    }

    void StartNextActivity() {
        if(currentIndex >= questions.size()) {
            Intent intent = new Intent(MainActivity.this, EndScreen.class);
            intent.putExtra("ru.greenbeard.totalQuestions", questions.size());
            intent.putExtra("ru.greenbeard.correctAnswers", correctAnswers);
            startActivity(intent);
        }
        else {
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            intent.putExtra("ru.greenbeard.currentIndex", currentIndex);
            intent.putExtra("ru.greenbeard.correctAnswers", correctAnswers);
            startActivity(intent);
        }
    }
}