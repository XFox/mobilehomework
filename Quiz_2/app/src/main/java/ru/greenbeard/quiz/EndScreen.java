package ru.greenbeard.quiz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class EndScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_screen);
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            int total = extras.getInt("ru.greenbeard.totalQuestions");
            int correct = extras.getInt("ru.greenbeard.correctAnswers");
            TextView result = findViewById(R.id.result);
            result.setText(String.format("%d/%d", correct, total));
        }
    }
}