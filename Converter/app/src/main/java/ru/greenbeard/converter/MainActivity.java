package ru.greenbeard.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.google.android.material.textfield.TextInputEditText;

import java.math.BigInteger;


public class MainActivity extends AppCompatActivity {
    private TextInputEditText decimalField = null;
    private TextInputEditText binaryField = null;
    private TextInputEditText hexField = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        decimalField = findViewById(R.id.DecimalField);
        binaryField = findViewById(R.id.BinaryField);
        hexField = findViewById(R.id.HexField);
    }

    public void Clear(View view) {
        decimalField.setText("");
        binaryField.setText("");
        hexField.setText("");
    }

    public void Convert(View view) {
        try {
            if (decimalField.getText().length() != 0) {
                BigInteger val = new BigInteger(decimalField.getText().toString(), 10);
                binaryField.setText(val.toString(2));
                hexField.setText(val.toString(16).toUpperCase());
            }
            else if (binaryField.getText().length() != 0) {
                BigInteger val = new BigInteger(binaryField.getText().toString(), 2);
                decimalField.setText(val.toString(10));
                hexField.setText(val.toString(16).toUpperCase());
            }
            else if (hexField.getText().length() != 0) {
                BigInteger val = new BigInteger(hexField.getText().toString(), 16);
                binaryField.setText(val.toString(2));
                decimalField.setText(val.toString(10));
            }
        } catch (Exception e) {
            Clear(null);
        }
    }
}