package ru.greenbeard.rssparser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void OnClick(View view) {
        XmlPullParser parser = getResources().getXml(R.xml.simple_rss);

        ArrayList<Spanned> stringArrayList = new ArrayList<>();
        try {
            /*XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();

            File file = new File(Environment.getExternalStorageState() + "/simple_xml.xml");
            if (!file.exists()) {
                Toast.makeText(this, "Этого файла по указанному пути не существует!", Toast.LENGTH_LONG);
                return;
            }

            FileInputStream fis = new FileInputStream(file);
            InputStreamReader reader = new InputStreamReader(fis);

            parser.setInput(reader);*/

            parser.next();
            String titleStr = "", linkStr = "", descrStr = "";
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT){
                if (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals("item")){
                    parser.next();
                    if (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals("title")) {
                        titleStr = parser.nextText();
                        parser.next();
                    }
                    if (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals("link")) {
                        linkStr = parser.nextText();
                        parser.next();
                    }
                    parser.next();
                    parser.next();
                    parser.next();
                    if (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals("description")) {
                        descrStr = parser.nextText();
                        parser.next();
                    }
                    stringArrayList.add(Html.fromHtml(String.format(getResources().getString(R.string.rss_item), titleStr, linkStr, descrStr), Html.FROM_HTML_MODE_LEGACY));
                }
                parser.next();
            }
        }
        catch(XmlPullParserException e){
            Toast.makeText(this, "Возникла ошибка типа XmlPullParserException", Toast.LENGTH_SHORT);
        }
        catch(IOException e){

        }
        ArrayAdapter<Spanned> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(adapter);
    }
}